﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiringController : MonoBehaviour {
	private float speed = 0.05f;
	private bool moving = false;
	public bool Moving {
		set{ moving = value; }
	}

	private Transform _transform;
	private Vector2 _currentPos;

	// Use this for initialization
	void Start () {
		_transform = gameObject.GetComponent<Transform> ();
		_currentPos = _transform.position;

		// Destroy itself when an animation is finished
		Destroy (gameObject, this.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).length); 
	}
	
	// Update is called once per frame
	void Update () {
		if (!moving)
			return;

		_currentPos = _transform.position;
		_currentPos += new Vector2 (+speed, 0);
		_transform.position = _currentPos;
	}
}
