﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * @source      Explosion Controller
 * @author      Sujin Byun
 * @created     2017-10-15
 * @modified    2017-10-15
 * @description Controls Explosion
 */

public class ExplosionController : MonoBehaviour {
	[SerializeField]
	private float speed = 0.05f;

	[SerializeField]
	private bool moving = true;

	private Transform _transform;
	private Vector2 _currentPos;

	void Start () {
		_transform = gameObject.GetComponent<Transform> ();
		_currentPos = _transform.position;

		// Destroy itself when an animation is finished
		Destroy (gameObject, this.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).length); 
	}

	// Move this to left
	void Update () {
		if (!moving)
			return;

		_currentPos = _transform.position;
		_currentPos += new Vector2 (-speed, 0);
		_transform.position = _currentPos;
	}
}
