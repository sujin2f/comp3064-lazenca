﻿/*
 * @source      HeartController
 * @author      Maksym Sen
 * @created     2017-12-20
 * @modified    2017-12-21
 * @description Controller for the Heart/Life game object.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartController : MonoBehaviour {

	[SerializeField]
	private float speed = 0.02f;
	[SerializeField]
	private float minX = 10f;
	[SerializeField]
	private float maxX = -7f;
	[SerializeField]
	private float minY = 4.5f;
	[SerializeField]
	private float maxY = -4.5f;


	private Transform _transform;
	public Vector2 _currentPos;

	private float time;

	void Start () {
		_transform = gameObject.GetComponent<Transform> ();
		_currentPos = _transform.position;
		time = Time.time;
	}

	void Update () {
		MoveItem ();
	}

	void MoveItem(){
		float timeDiff = Time.time - time;
		float x = (Mathf.Pow (timeDiff - 0.1f, 2) - 0.1f) * 0.3f;

		_currentPos = transform.transform.position;
		_currentPos -= new Vector2 (x, 0);
		if (_currentPos.x < maxX) {
			Destroy (gameObject);
		}
		_transform.position = _currentPos;
	}
}
