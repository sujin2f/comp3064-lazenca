﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombController : MonoBehaviour {

	[SerializeField]
	private float speed = 0.01f;
	[SerializeField]
	private float maxX = -7f;

	private Transform _transform;
	public Vector2 _currentPos;

	private float time;

	void Start () {
		_transform = gameObject.GetComponent<Transform> ();
		_currentPos = _transform.position;
		time = Time.time;
	}

	void Update () {
		float timeDiff = Time.time - time;
		float x = (Mathf.Pow (timeDiff - 0.1f, 2) - 0.1f) * 0.3f;

		_currentPos = transform.transform.position;
		_currentPos -= new Vector2 (x, 0);
		if (_currentPos.x < maxX) {
			Destroy (gameObject);
		}
		_transform.position = _currentPos;
	}
}
