﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * @source      Player
 * @author      Sujin Byun
 * @created     2017-10-16
 * @modified    2017-12-19
 * @description Singleton class to store game data
 */

public class Game {
	// Instance
	static private Game _instance;
	static public Game Instance{
		get { 
			if (_instance == null)
				_instance = new Game ();

			return _instance;
		}
	}
	private Game() {}

	public ISceneController currentSceneController;

	private float _boundX;
	public float BoundX{
		get{ return _boundX; }
		set{ _boundX = value; }
	}
	private float _boundY;
	public float BoundY{
		get{ return _boundY; }
		set{ _boundY = value; }
	}

	private float _bgPositionX;
	public float BgPositionX{
		get{ return _bgPositionX; }
		set{ _bgPositionX = value; }
	}
	private float _planetPositionX;
	public float PlanetPositionX{
		get{ return _planetPositionX; }
		set{ _planetPositionX = value; }
	}

	private int _score = 0;
	public int Score{
		get{ return _score; }
		set{ 
			_score = value;
			if (_score > _highScore)
				_highScore = _score;
			
			currentSceneController.UpdateUI();
		}
	}

	private int _highScore = 0;
	public int HighScore {
		get{ return _highScore; }
	}

	private int _life = 3;
	public int Life{
		get{ return _life; }
		set{ 
			if (!_immortal) {
				_life = value;
				currentSceneController.SetLife (_life);

				if (_life <= 0) {
					//game over
					currentSceneController.GameOver();
				}else{
					currentSceneController.UpdateUI();
				}
			}
		}
	}

	private int _bomb = 3;
	public int Bomb{
		get{ return _bomb; }
		set{ 
			_bomb = value;
			currentSceneController.SetBomb (_bomb);
		}
	}

	private int _numEnemy = 0;
	public int NumEnemy{
		get{ return _numEnemy; }
		set{ _numEnemy = value; }
	}

	private bool _bossEmerging = true;
	public bool BossEmerging {
		get{ return _bossEmerging; }
		set{ _bossEmerging = value; }
	}

	private int _bossEnergy;
	public int BossEnergy{
		get{ return _bossEnergy; }
		set{ _bossEnergy = value; }
	}

	private bool _immortal;
	public bool Immortal{
		get{ return _immortal; }
		set{ _immortal = value; }
	}
}
