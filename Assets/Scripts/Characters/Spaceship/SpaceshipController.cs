﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * @source      Spaceship Controller
 * @author      Sujin Byun
 * @created     2017-10-15
 * @modified    2017-12-19
 * @description Controls spaceship by user
 */

public class SpaceshipController : MonoBehaviour {
	// Public variables
	[SerializeField]
	private GameObject laser;

	[SerializeField]
	private GameObject bomb;

	[SerializeField]
	private float speed = 0.15f;

	// Private variables
	private float topY;
	private float bottomY;

	private float leftX;
	private float rightX;

	private float boundY = 0.9f;
	private float boundX = 0.9f;

	private Transform _transform;
	private Vector2 _currentPos;

	public Vector2 CurrentPos{
		get { 
			return _currentPos;
		}
	}


	// Use this for initialization
	void Start () {
		_transform = gameObject.GetComponent<Transform> ();
		_currentPos = _transform.position;
	}

	void SetBounce() {
		topY = Game.Instance.BoundY - boundY;
		bottomY = topY * -1;

		rightX = Game.Instance.BoundX - boundX;
		leftX = rightX * -1;
	}		
	
	// Update is called once per frame
	void Update () {
		if (Game.Instance.currentSceneController != null) {
			SetBounce ();
		}

		UpdatePosition ();
		FireLaser ();
		FireBomb ();
	}

	private void UpdatePosition() {
		_currentPos = _transform.position;

		float userInput = Input.GetAxis ("Vertical");

		if(userInput > 0) { 
			// Move up
			_currentPos += new Vector2 (0, speed);
		} else if (userInput < 0) { 
			// Move down
			_currentPos -= new Vector2 (0, speed);
		}

		userInput = Input.GetAxis ("Horizontal");

		if(userInput > 0) { 
			// Move up
			_currentPos += new Vector2 (speed, 0);
		} else if (userInput < 0) { 
			// Move down
			_currentPos -= new Vector2 (speed, 0);
		}

		CheckBounds ();

		// Apply the position
		_transform.position = _currentPos;
	}

	private void FireLaser() {
		if (Input.GetKeyDown ("space") || Input.GetKeyDown ("j")) {
			// Create Laser
			Instantiate (laser, _transform.position, _transform.rotation);
		}
	}

	private void FireBomb() {
		if (Input.GetKeyDown ("k") && Game.Instance.Bomb > 0) {
			// Create Bomb
			float angle = -90;
			Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
			Instantiate (bomb, _transform.position, q);
			Game.Instance.Bomb--;
		}
	}

	// Check y bound
	private void CheckBounds(){
		if (_currentPos.y < bottomY) {
			// Limit bottom
			_currentPos.y = bottomY;
		} else if (_currentPos.y > topY) {
			// Limit top
			_currentPos.y = topY;
		} else if (_currentPos.x > rightX) {
			// Limit right
			_currentPos.x = rightX;
		} else if (_currentPos.x < leftX) {
			// Limit left
			_currentPos.x = leftX;
		}
	}
}
