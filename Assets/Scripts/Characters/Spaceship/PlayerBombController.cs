﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBombController : MonoBehaviour {
	[SerializeField]
	private float spawningSpeed = 1;

	// Private variables
	private Transform _transform;
	private Vector2 _currentPos;

	private float destX;

	// Use this for initialization
	void Start () {
		_transform = gameObject.GetComponent<Transform> ();
		_currentPos = _transform.position;
		destX = _currentPos.x + Game.Instance.BoundX + 2;
	}
	
	// Update is called once per frame
	void Update () {
		float addX = (destX - _currentPos.x) / 10;

		if (addX < 0.05) {
			Game.Instance.currentSceneController.BombExplode ();
			Destroy (gameObject);
		} else {
			// Set position
			_currentPos = new Vector2 (_currentPos.x + addX, _currentPos.y);
			_transform.position = _currentPos;
		}			
	}
}
