﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * @source      Spaceship Collision Contoller
 * @author      Sujin Byun
 * @created     2017-10-16
 * @modified    2017-10-16
 * @description Controls collision for spaceship
 */

[RequireComponent(typeof(AudioSource))]
public class SpaceshipCollision : MonoBehaviour {
	// Explosion Animation
	[SerializeField]
	private GameObject explosion;

	// Collision occured
	public void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag.Equals ("bossLaser") && !other.gameObject.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsTag ("Fire"))
			return;

		// Hits by enemy laser
		if (other.gameObject.tag.Equals("EnemyLaser") || other.gameObject.tag.Equals("bossMissile") || other.gameObject.tag.Equals("bossLaser") || other.gameObject.tag.Equals("Enemy2")) {
			// Create Explosion at the right position
			Vector2 _currentPos = new Vector2 (other.gameObject.transform.position.x - 1.5f, other.gameObject.transform.position.y);
			Instantiate (explosion, _currentPos, other.gameObject.transform.rotation);

			// Descrese Life
			Game.Instance.Life--;

			// Show hit effect (red background)
			StartCoroutine(Game.Instance.currentSceneController.ShowHitEffect());

			DestroyObject (other.gameObject);
			Destroy (other.gameObject);

			// Game over
			if (Game.Instance.Life <= 0) {
				DestroyObject (gameObject);
			}
		}

		if (other.gameObject.tag == "Heart"){
			AudioSource audio = GetComponent<AudioSource>();
			audio.Play();

			if (Game.Instance.Life < 10) {
				// Increase health on pickup	
				Game.Instance.Life++;
			}

			DestroyObject (other.gameObject);
			Destroy (other.gameObject);
		}

		if (other.gameObject.tag == "Bomb") {
			AudioSource audio = GetComponent<AudioSource>();
			audio.Play();

			if (Game.Instance.Bomb < 10) {
				// Increase health on pickup	
				Game.Instance.Bomb++;
			}

			DestroyObject (other.gameObject);
			Destroy (other.gameObject);
		}
	}
}
