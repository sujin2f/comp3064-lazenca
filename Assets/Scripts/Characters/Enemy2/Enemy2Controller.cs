﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * @source      Enemy Controller
 * @author      Sujin Byun
 * @created     2018-01-11
 * @modified    2018-01-11
 * @description Controls enemy2 instant
 */

public class Enemy2Controller : MonoBehaviour {

	[SerializeField]
	private float speed = 0.2f;

	[SerializeField]
	private float spawningXLimit = 0.5f;
	private float spawnPointX;

	// Private variables
	private Transform _transform;
	private Vector2 _currentPos;

	// Coordination
	private float endX;
	private float topY;
	private float bottomY;
	private float boundY = 0.9f;
	private float vibrationWidth = 0.05f;
	private float decimalPoint = 1000f; // How much devide the screen

	// Use this for initialization
	void Start () {
		_transform = gameObject.GetComponent<Transform> ();
		_currentPos = _transform.position;

		// Set bound from setting
		topY = Game.Instance.BoundY - boundY;
		bottomY = topY * -1;

		// Set spawning point
		InitSpawn();
	}

	private void InitSpawn() {
		endX = (Game.Instance.BoundX + 2) * -1;

		// Set spawning point y
		float limitTop = Game.Instance.BoundY - 1; // destination limit top/bottom
		int topBound = (int) Mathf.Ceil(limitTop * decimalPoint); // make it bigger for making it float number
		int randomY = Random.Range (0, topBound * 2) - topBound; // multiply by 2 and minus it (-x.x ~ x.x)
		float spawnPointY = ((float) randomY) / decimalPoint; // get float number

		// Set spawning point x
		spawnPointX = Game.Instance.BoundX + 1f; // out of screen (starting point)

		// Set position
		_currentPos = new Vector2 (spawnPointX, spawnPointY);
		_transform.position = _currentPos;
	}

	// Update is called once per frame
	void Update () {
		_currentPos = _transform.position;

		float y = Mathf.Sin(Time.time * 3) * vibrationWidth;

		_currentPos -= new Vector2 (speed, y);
		_transform.position = _currentPos;

		// Destroy this when it goes to out of bound
		if (_currentPos.x <= endX)
			DestroyObject (gameObject);
	}
}
