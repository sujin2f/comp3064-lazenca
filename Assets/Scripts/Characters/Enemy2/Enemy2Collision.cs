﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * @source      Enemy Collision
 * @author      Sujin Byun
 * @created     2018-01-11
 * @modified    2018-01-11
 * @description Controls enemy-2 collision
 */

public class Enemy2Collision : MonoBehaviour {
	[SerializeField]
	private GameObject explosion;
	[SerializeField]
	private GameObject bomb;
	[SerializeField]
	private GameObject life;

	public void OnTriggerEnter2D(Collider2D other) {
		// Hits by laser
		if (other.gameObject.tag.Equals("laser")) {
			DestroyThis ();
			DestroyObject (other.gameObject);
			Destroy (other.gameObject);
		}
	}
	public void DestroyThis() {
		// Create Explosion
		Vector2 _currentPos = new Vector2 (transform.position.x, transform.position.y);
		Instantiate (explosion, _currentPos, transform.rotation);

		// Increase Score
		Game.Instance.Score += 10;


		float x = Random.Range(0,101);
		float item = Random.Range(0,2);
		 
		if (x < 20) {
			// Drops bombs at 20% chance
			if (item == 1) {
				Instantiate (bomb, _currentPos, transform.rotation);
			} else {
				Instantiate (life, _currentPos, transform.rotation);
			}
		}

		// Delete Objects (Enemy and Laser)
		DestroyObject (gameObject);
	}
}
