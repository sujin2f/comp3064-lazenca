﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * @source      Enemy Collision
 * @author      Sujin Byun
 * @created     2017-10-15
 * @modified    2017-10-15
 * @description Controls enemy-laser collision
 */

public class EnemyCollision : MonoBehaviour {
	[SerializeField]
	private GameObject explosion;
	[SerializeField]
	private GameObject bomb;
	[SerializeField]
	private GameObject life;

	public void OnTriggerEnter2D(Collider2D other) {
		// Hits by laser
		if (other.gameObject.tag.Equals("laser")) {
			// Prevent to destroied before it appears
			if (!gameObject.GetComponent<EnemyController> ().IsReady)
				return;

			DestroyThis ();

			DestroyObject (other.gameObject);
			Destroy (other.gameObject);
		}
	}

	public void DestroyThis() {
		// Decrease Num Enemies
		Game.Instance.NumEnemy--;

		// Create Explosion
		Vector2 _currentPos = new Vector2 (transform.position.x, transform.position.y);
		Instantiate (explosion, _currentPos, transform.rotation);

		// Increase Score
		Game.Instance.Score += 10;

		float chance = Random.Range(0,101);
		float item = Random.Range(0,2);

		if (chance < 20) {
			// Drops bombs/ life at 20% chance
			if (item == 1) {
				Instantiate (bomb, _currentPos, transform.rotation);
			} else {
				Instantiate (life, _currentPos, transform.rotation);
			}
		}

		// Delete Objects (Enemy and Laser)
		DestroyObject (gameObject);
	}
}
