﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * @source      Enemy Controller
 * @author      Sujin Byun
 * @created     2017-10-15
 * @modified    2017-10-15
 * @description Controls enemy instant
 */

public class EnemyController : MonoBehaviour {
	[SerializeField]
	private GameObject laser;
	[SerializeField]
	private float speed = 0.05f;
	[SerializeField]
	private int shootingChance = 80; // Larger number makes it rare
	[SerializeField]
	private int dirChangeChance = 100; // Larger number makes it rare

	// Spawn variables
	[SerializeField]
	private float spawningSpeed = 3;
	[SerializeField]
	private float spawningXLimit = 0.5f;
	private float spawnPointX;
	private float spawnDestX;
	private float frameCount = 0;
	private float appearanceGap; // From spawning point to destination point
	private bool isReady = false; // Ready to fire. Immortal when this is false.
	private float decimalPoint = 1000f; // How much devide the screen

	// Private variables
	private Transform _transform;
	private Vector2 _currentPos;

	// Coordination
	private float topY;
	private float bottomY;
	private float boundY = 0.9f;

	private int direction; // 1: move top / 2: move bottom

	public bool IsReady {
		get { return isReady; }
	}

	private void InitSpawn() {
		// Set spawning point y
		float limitTop = Game.Instance.BoundY - 1; // destination limit top/bottom
		int topBound = (int) Mathf.Ceil(limitTop * decimalPoint); // make it bigger for making it float number
		int randomY = Random.Range (0, topBound * 2) - topBound; // multiply by 2 and minus it (-x.x ~ x.x)
		float spawnPointY = ((float) randomY) / decimalPoint; // get float number

		// Set spawning point x
		spawnPointX = Game.Instance.BoundX + 1f; // out of screen (starting point)

		float limitRight = Game.Instance.BoundX - 1.2f; // destination limit right
		int rightBound = (int) Mathf.Ceil(limitRight * decimalPoint); // make it bigger for making it float number
		int leftBound = (int) Mathf.Ceil(spawningXLimit * decimalPoint); // make it bigger for making it float number
		int randomX = Random.Range (leftBound, rightBound);
		spawnDestX = ((float)randomX) / decimalPoint;

		// Set position
		_currentPos = new Vector2 (spawnPointX, spawnPointY);
		_transform.position = _currentPos;
	}

	// Use this for initialization
	void Start () {
		_transform = gameObject.GetComponent<Transform> ();
		_currentPos = _transform.position;

		// Set bound from setting
		topY = Game.Instance.BoundY - boundY;
		bottomY = topY * -1;

		// Set spawning point
		InitSpawn();

		// initialize private values
		direction = Random.Range (0, 2) == 0 ? -1 : 1;
	}
	
	void Update () {
		if (frameCount >= 90) {
			isReady = true;
		} else {
			// if framecount is under 90 (degree), continue spawning
			Appear ();
		}

		// when it ready, fire and move
		if (isReady == true) {
			FireLaser ();
			Move ();
		}
			
	}

	// Appearance Animation
	private void Appear() {
		// frameCount is same as degree
		float radian = frameCount * (Mathf.PI / 180);
		// set new position with sine value (ease in)
		float positionX = spawnPointX - (Mathf.Sin (radian) * (spawnPointX - spawnDestX));

		// Set position
		_currentPos = new Vector2 (positionX, _currentPos.y);
		_transform.position = _currentPos;

		frameCount += spawningSpeed;
	}

	// Create Enemy Laser
	private void FireLaser() {
		// get random chance
		if (Random.Range (0, shootingChance) != 1)
			return;

		Instantiate (laser, _transform.position, _transform.rotation);
	}

	private void Move() {
		// get random chance
		if (Random.Range (0, dirChangeChance) == 1)
			direction *= -1;

		_currentPos = _transform.position;
		_currentPos += new Vector2 (0, speed) * direction;

		CheckBounds ();

		_transform.position = _currentPos;
	}

	private void CheckBounds(){
		if (_currentPos.y < bottomY) {
			_currentPos.y = bottomY;
			direction *= -1;
		} else if (_currentPos.y > topY) {
			_currentPos.y = topY;
			direction *= -1;
		}
	}
}
