﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * @source      Planet Controller
 * @author      Sujin Byun
 * @created     2017-10-15
 * @modified    2017-10-15
 * @description Controls foreground
 */

public class PlanetController : MonoBehaviour {
	// Public variables
	[SerializeField]
	private float speed = 0.009f;
	[SerializeField]
	private float startX = 38.78f;
	[SerializeField]
	private float endX = -36.31f;

	// Private variables
	private Transform _transform;
	private Vector2 _currentPos;

	public float PositionX{
		get{ return _transform.position.x; }
	}

	// Use this for initialization
	void Start () {
		_transform = gameObject.GetComponent<Transform> ();
		_currentPos = _transform.position;
		Init ();
	}

	// Update is called once per frame
	void Update () {
		_currentPos = _transform.position;
		// Move space left
		_currentPos -= new Vector2 (speed, 0);

		// check if we need to reset
		if (_currentPos.x < endX) {
			// reset
			Reset ();
		}
		// apply changes
		_transform.position = _currentPos;
	}

	// Reset background position
	private void Reset(){
		_currentPos = new Vector2 (startX, 0);
		_transform.position = _currentPos;
	}

	private void Init() {
		float x;

		if (Game.Instance.PlanetPositionX == default(float))
			x = startX;
		else
			x = Game.Instance.PlanetPositionX;

		_currentPos = new Vector2 (x, 0);
		_transform.position = _currentPos;
	}
}
