﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*
 * @source      Level1Controller
 * @author      Sujin Byun
 * @created     2017-12-19
 * @modified    2017-12-19
 * @description 
 */

public class Level1Controller : LevelController {
	[SerializeField]
	private float spawnTime = 1f;

	// Boss Settings
	[SerializeField]
	private float spawnBossTime = 1f;
	[SerializeField]
	bool immortal;

	private void Initialize(){
		// initialize the score and life
		Game.Instance.Score = 0;
		SetLife (Game.Instance.Life);
		SetBomb (Game.Instance.Bomb);

		LevelStart ();
	}

	void Start () {
		Game.Instance.currentSceneController = this;
		Initialize ();

		Game.Instance.BoundX = 8;
		Game.Instance.BoundY = 5;

		Game.Instance.Immortal = immortal;
	}

	public void Spawn() {
		// spawn enemies
		InvokeRepeating ("SpawnEnemy", spawnTime, spawnTime);
		Invoke("SpawnBoss", spawnBossTime);
	}

	void Update() {
		if (levelClear && Game.Instance.NumEnemy < 1) {
			levelClear = false;
			Invoke ("ToLevel2", 2);
		}
	}

	private void ToLevel2() {
		Game.Instance.BgPositionX = GameObject.FindGameObjectWithTag ("Background").GetComponent<BackgroundController> ().PositionX;
		Game.Instance.PlanetPositionX = GameObject.FindGameObjectWithTag ("Planet").GetComponent<PlanetController> ().PositionX;

		SceneManager.LoadScene ("Level2");
	}
}
