﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * @source      Level1TextController
 * @author      Sujin Byun
 * @created     2017-12-19
 * @modified    2017-12-19
 * @description Controls the text 'Level 1'
 */

// https://docs.unity3d.com/ScriptReference/Material-color.html
public class Level1TextController : MonoBehaviour {
	private Color colorStart = Color.white;
	private Color colorEnd = Color.clear;

	private Renderer rend;
	private int segment;
	private float time;

	// Use this for initialization
	void Start () {
		segment = 0;
		time = Time.time;
		rend = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
		float timeDiff = Time.time - time;

		if (segment == 0 && timeDiff > 1) {
			time = Time.time;
			segment = 1;

		} else if (segment == 1) {
			float lerp = Mathf.Sin(timeDiff);
			rend.material.color = Color.Lerp(colorStart, colorEnd, lerp);

			if (lerp > 0.999f) {
				time = Time.time;
				segment = 2;
			}
		
		} else if (segment == 2 && timeDiff > 1) {
			(Game.Instance.currentSceneController as Level1Controller).Spawn();
			Destroy (gameObject);
		}
	}
}
