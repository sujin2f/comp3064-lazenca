﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * @source      Enemy Collision
 * @author      Sujin Byun
 * @created     2017-12-18
 * @modified    2017-12-18
 * @description Controls Boss collision
 */


public class BossCollision : MonoBehaviour {
	[SerializeField]
	private GameObject explosion;

	[SerializeField]
	private int _energy = 30;

	private int numParts;
	private int expParts = 0;
	private int fullEnergy;

	private List<Explodable> _explodable = new List<Explodable>();

	void Start () {
		GameObject[] gos = GameObject.FindGameObjectsWithTag ("boss_body");

		foreach (GameObject go in gos) {
			Explodable exp = go.GetComponent<Explodable> ();
			if (exp != null) {
				_explodable.Add(exp);
			}
		}

		numParts = _explodable.Count;
		Game.Instance.BossEnergy = _energy;
		fullEnergy = _energy;
	}

	public void OnTriggerEnter2D(Collider2D other) {
		// Hits by laser
		if (other.gameObject.tag.Equals("laser")) {
			// Prevent to destroied before it appears
			if (Game.Instance.BossEmerging)
				return;

			// Decrease Energy
			Game.Instance.BossEnergy--;

			int part = Mathf.Abs ((numParts + 1) * Game.Instance.BossEnergy / fullEnergy);

			if (expParts != part && part >= 0 && part < numParts) {
				_explodable [part].explode ();
				expParts = part;
			}

			// Create Explosion
			Vector2 _currentPos = new Vector2 (other.gameObject.transform.position.x + 2, other.gameObject.transform.position.y);
			Instantiate (explosion, _currentPos, other.gameObject.transform.rotation);

			// Increase Score
			Game.Instance.Score += 10;

			// Delete Objects (Laser)
			DestroyObject (other.gameObject);
			Destroy (other.gameObject);

			if (Game.Instance.BossEnergy <= 0) {
				Game.Instance.currentSceneController.DefeatBoss ();
			}	
		}
	}

	// https://answers.unity.com/questions/16097/order-of-gameobjectfindgameobjectswithtagstring-ta.html
	int CompareObNames( GameObject x, GameObject y )
	{
		return x.name.CompareTo( y.name );
	}

}
