﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTurret : MonoBehaviour {
	[SerializeField]
	private float speed = 1.00f;

	[SerializeField]
	private float spawnTime = 1f;

	[SerializeField]
	private float delay = 0.1f;

	[SerializeField]
	private GameObject missile;

	[SerializeField]
	private GameObject fire;

	private Transform _transform;
	private Quaternion _rotation;
	private Vector2 _currentPos;
	private float angle;

	// Use this for initialization
	void Start () {
		_transform = gameObject.GetComponent<Transform> ();
		_currentPos = _transform.position;
		_rotation = _transform.rotation;

		// spawn missile
		InvokeRepeating ("SpawnMissile", spawnTime + delay, spawnTime);
	}
	
	// Update is called once per frame
	void Update () {
		FaceTo ();
	}

	public void FaceTo() {
		// https://answers.unity.com/questions/650460/rotating-a-2d-sprite-to-face-a-target-on-a-single.html
		_currentPos = _transform.position;
		_rotation = _transform.rotation;
 
		if (GameObject.FindWithTag ("Player") == null)
			return;

		Vector3 vectorToTarget = GameObject.FindWithTag("Player").GetComponent<SpaceshipController> ().CurrentPos - _currentPos;

		angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
		Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
		_rotation = Quaternion.Slerp(_rotation, q, Time.deltaTime * speed);

		_transform.rotation = _rotation;
	}

	// Spawn missile
	void SpawnMissile () {
		if (Game.Instance.BossEmerging)
			return;

		float _angle = angle;
		Quaternion q = _rotation * Quaternion.Euler (0, 0, -90);

		// Spawn! and set the angle
		GameObject _missile;
		Vector2 missilePos = _currentPos;
		missilePos += new Vector2 (0.3f * Mathf.Cos((_angle * Mathf.PI)/180), 0.3f * Mathf.Sin((_angle * Mathf.PI)/180));

		_missile = Instantiate (missile, missilePos, q);
		_missile.GetComponent<MissileController> ().Angle = (_angle * Mathf.PI)/180;

		// explosion effect
		Instantiate (fire, missilePos, q);
	}
}
