﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileController : MonoBehaviour {
	// Public variables
	[SerializeField]
	private float initialSpeed = 0.12f;

	// Private variables
	private Transform _transform;
	private Vector2 _currentPos;

	private float endX;
	private float endYTop;
	private float endYBottom;
	private float angle;

	private float speed = 0f;
	private int frameCount = 0;

	public float Angle{
		set{ 
			angle = value;
		}
	}

	void SetBounce() {
		endX = Game.Instance.BoundX + 2;
		endYTop = Game.Instance.BoundY + 2;
		endYBottom = endYTop * -1;
	}		

	// Use this for initialization
	void Start () {
		_transform = gameObject.GetComponent<Transform> ();
		_currentPos = _transform.position;
	}

	// Update is called once per frame
	void Update () {
		if (Game.Instance.currentSceneController != null) {
			SetBounce ();
		}

		if (frameCount < 90) {
			float radian = frameCount * (Mathf.PI / 180);
			speed = Mathf.Sin (radian) * initialSpeed;
			frameCount++;
		}

		float x = Mathf.Cos (angle) * speed;
		float y = Mathf.Sin (angle) * speed;

		_currentPos = _transform.position;
		_currentPos += new Vector2 (x, y);
		_transform.position = _currentPos;

		// Destroy this when it goes to out of bound
		if (_currentPos.x >= endX || _currentPos.y >= endYTop || _currentPos.y <= endYBottom)
			DestroyObject (gameObject);
	}
}
