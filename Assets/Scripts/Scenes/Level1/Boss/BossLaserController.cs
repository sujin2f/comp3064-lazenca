﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossLaserController : MonoBehaviour {
	void Update() {
		if (this.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsTag ("Destroy")) {
			Destroy (gameObject);
		}			
	}
}
