﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceController : MonoBehaviour {
	private float endX;

	void Start () {
		endX = (Game.Instance.BoundX + 2);
	}

	void Update () {
		// Destroy this when it goes to out of bound
		if (transform.position.x >= endX)
			DestroyObject (gameObject);
	}
}
