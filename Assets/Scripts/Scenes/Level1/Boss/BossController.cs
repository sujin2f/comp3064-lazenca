﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * @source      Boss Controller
 * @author      Sujin Byun
 * @created     2017-12-17
 * @created     2017-12-17
 * @description Controls Boss Character
 */

public class BossController : MonoBehaviour {
	[SerializeField]
	private float spawningSpeed = 3;

	[SerializeField]
	private GameObject laser;

	// Private variables
	private Transform _transform;
	private Vector2 _currentPos;

	private float frameCount = 0;

	[SerializeField]
	private int shootingChance = 80; // Larger number makes it rare

	private GameObject laserFiring;
	private bool explosion = false;

	// Use this for initialization
	void Start () {
		_transform = gameObject.GetComponent<Transform> ();
		_currentPos = _transform.position;

		// Set spawning point
		InitSpawn();
	}
	
	private void InitSpawn() {
		// Set position
		_currentPos = new Vector2 (12.92f, 0);
		_transform.position = _currentPos;
	}

	// Update is called once per frame
	void Update () {
		if (frameCount < 90) {
			Appear ();
		} else {
			if (Game.Instance.BossEmerging)
				Game.Instance.BossEmerging = false;

			if (Game.Instance.BossEnergy <= 0) {
				DefeatBoss ();
			} else {
				FireLaser ();
			}
		}
	}

	private void Appear() {
		// frameCount is same as degree
		float radian = frameCount * (Mathf.PI / 180);
		// set new position with sine value (ease in)
		float positionX = 12.92f - (Mathf.Sin (radian) * (12.92f - 5.969595f));

		// Set position
		_currentPos = new Vector2 (positionX, _currentPos.y);
		_transform.position = _currentPos;

		frameCount += spawningSpeed;
	}

	// Create Enemy Laser
	private void FireLaser() {
		if (laserFiring != null)
			return;

		// get random chance
		if (Random.Range (0, shootingChance) != 1)
			return;

		Vector2 laserPosition = new Vector2 (-3.79f, 0f);

		laserFiring = Instantiate (laser, laserPosition, _transform.rotation);
	}

	public void DefeatBoss() {
		explosion = true;
	}
}
