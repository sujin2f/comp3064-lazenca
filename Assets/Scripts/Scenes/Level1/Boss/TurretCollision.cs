﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * @source      Enemy Collision
 * @author      Sujin Byun
 * @created     2017-12-17
 * @modified    2017-12-17
 * @description Controls enemy-laser collision
 */

public class TurretCollision : MonoBehaviour {
	[SerializeField]
	private GameObject explosion;

	[SerializeField]
	private bool explosive = true;

	[SerializeField]
	private float life = 3;

	public void OnTriggerEnter2D(Collider2D other) {
		if (Game.Instance.BossEmerging)
			return;

		if (!explosive)
			return;

		// Hits by laser
		if (other.gameObject.tag.Equals("laser")) {
			// Decrease Life
			life--;

			// Create Explosion
			Vector2 _currentPos = new Vector2 (other.gameObject.transform.position.x + 2, other.gameObject.transform.position.y);
			Instantiate (explosion, _currentPos, other.gameObject.transform.rotation);

			// Increase Score
			Game.Instance.Score += 10;

			// Delete Objects (Turret and Laser)
			if (life == 0) {
				DestroyObject (gameObject);
			}

			DestroyObject (other.gameObject);
			Destroy (other.gameObject);
		}
	}
}
