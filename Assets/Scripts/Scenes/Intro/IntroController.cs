﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
 * @source      Intro Scene Controller
 * @author      Sujin Byun
 * @created     2017-12-19
 * @modified    2017-12-19
 * @description Controls Scene
 */

public class IntroController : MonoBehaviour, ISceneController {
	// Num of Life
	[SerializeField]
	int life;
	// Screen Bound
	[SerializeField]
	public float boundX = 8;
	[SerializeField]
	public float boundY = 5;

	[SerializeField]
	GameObject logo;
	[SerializeField]
	GameObject keyboard;
	[SerializeField]
	Text pressKey;
	[SerializeField]
	bool immortal;

	private bool modeKeyboard = false;

	// Use this for initialization
	void Start () {
		Game.Instance.currentSceneController = this;

		Game.Instance.Life = life;
		Game.Instance.BoundX = boundX;
		Game.Instance.BoundY = boundY;
		Game.Instance.Immortal = immortal;

		SetModeLogo ();
	}

	// Update is called once per frame
	void Update() {
		if (!modeKeyboard && Input.GetKeyDown (KeyCode.K)) {
			modeKeyboard = true;
			SetModeKeyboard ();
		} else if (!modeKeyboard && Input.GetKeyDown (KeyCode.J)) {
			ToLevel1 ();
		} else if (modeKeyboard && Input.GetKeyDown (KeyCode.K)) {
			modeKeyboard = false;
			SetModeLogo ();
		}
	}

	private void SetModeLogo() {
		logo.gameObject.SetActive (true);
		keyboard.gameObject.SetActive (false);
		pressKey.gameObject.SetActive (true);
	}	

	private void SetModeKeyboard() {
		logo.gameObject.SetActive (false);
		keyboard.gameObject.SetActive (true);
		pressKey.gameObject.SetActive (false);
	}	

	private void ToLevel1() {
		Game.Instance.BgPositionX = GameObject.FindGameObjectWithTag ("Background").GetComponent<BackgroundController> ().PositionX;
		Game.Instance.PlanetPositionX = GameObject.FindGameObjectWithTag ("Planet").GetComponent<PlanetController> ().PositionX;

		SceneManager.LoadScene ("Level1");
	}

	public void UpdateUI(){}
	public IEnumerator ShowHitEffect() { return null; }
	public void SetLife(int life) {}
	public void SetBomb(int bomb) {}
	public void GameOver() {}
	public void DefeatBoss() {}
	public void Spawn() {}
	public void BombExplode() {}
}
