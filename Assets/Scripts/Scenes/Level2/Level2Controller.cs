﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*
 * @source      Level2Controller
 * @author      Sujin Byun
 * @created     2018-01-11
 * @modified    2018-01-11
 * @description 
 */

public class Level2Controller : LevelController {
	[SerializeField]
	private float spawnEnemy1Time = 1f;
	[SerializeField]
	private float spawnEnemy2Time = 1f;

	// Boss Settings
	[SerializeField]
	private float spawnBossTime = 1f;
	[SerializeField]
	bool immortal;

	// Enemy Settings
	[SerializeField]
	private GameObject enemy2;

	private void Initialize(){
		// initialize the score and life
		SetLife (Game.Instance.Life);
		SetBomb (Game.Instance.Bomb);

		LevelStart ();
	}

	void Start () {
		Game.Instance.currentSceneController = this;
		Initialize ();
		Game.Instance.Immortal = immortal;
	}

	public void Spawn() {
		// spawn enemies
		InvokeRepeating ("SpawnEnemy", spawnEnemy1Time, spawnEnemy1Time);
		InvokeRepeating ("Spawn2Enemy", spawnEnemy2Time, spawnEnemy2Time);
		Invoke("SpawnBoss", spawnBossTime);
	}

	void Update() {
		if (levelClear && Game.Instance.NumEnemy < 1) {
			levelClear = false;
			Invoke ("GameClear", 2);
		}
	}

	void Spawn2Enemy() {
		Instantiate (enemy2);
	}
}
