﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISceneController {
	void UpdateUI();
	void SetLife(int life);
	void SetBomb(int bomb);
	void GameOver();
	IEnumerator ShowHitEffect();
	void DefeatBoss();
	void BombExplode();
}
