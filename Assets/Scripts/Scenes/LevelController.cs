﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour, ISceneController {
	// UI
	[SerializeField]
	GameObject lifeImage;
	[SerializeField]
	GameObject bombImage;
	[SerializeField]
	Text scoreLabel;
	[SerializeField]
	Image gameOverImage;
	[SerializeField]
	Image gameClearImage;
	[SerializeField]
	Image hitEffect;
	[SerializeField]
	Text highScoreLabel;
	[SerializeField]
	Button resetBtn;

	// Enemy Settings
	[SerializeField]
	private GameObject enemy;
	[SerializeField]
	private int maxEnemy = 10;
	[SerializeField]
	private GameObject boss;

	protected bool levelClear = false;

	// Gameover Action
	public void GameOver(){
		// change high score text
		highScoreLabel.text = "HIGH SCORE : " + Game.Instance.HighScore;

		// hide/show texts/images
		gameOverImage.gameObject.SetActive (true);
		highScoreLabel.gameObject.SetActive (true);
		resetBtn.gameObject.SetActive (true);
		scoreLabel.gameObject.SetActive (false);

		gameClearImage.gameObject.SetActive (false);
	}

	protected void LevelStart() {
		// hide/show texts/images
		gameOverImage.gameObject.SetActive (false);
		gameClearImage.gameObject.SetActive (false);
		highScoreLabel.gameObject.SetActive (false);
		resetBtn.gameObject.SetActive (false);
		hitEffect.gameObject.SetActive (false);
		scoreLabel.gameObject.SetActive (true);
	}

	protected void GameClear() {
		// change high score text
		highScoreLabel.text = "HIGH SCORE : " + Game.Instance.HighScore;

		// hide/show texts/images
		highScoreLabel.gameObject.SetActive (true);
		resetBtn.gameObject.SetActive (true);
		scoreLabel.gameObject.SetActive (false);

		gameOverImage.gameObject.SetActive (false);
		gameClearImage.gameObject.SetActive (true);
	}

	// Change Score
	public void UpdateUI(){
		scoreLabel.text = Game.Instance.Score.ToString();
	}

	// Reset button callback
	public void ResetBtnClick(){
		Game.Instance.Life = 3;
		Game.Instance.Bomb = 3;

		Game.Instance.BgPositionX = GameObject.FindGameObjectWithTag ("Background").GetComponent<BackgroundController> ().PositionX;
		Game.Instance.PlanetPositionX = GameObject.FindGameObjectWithTag ("Planet").GetComponent<PlanetController> ().PositionX;

		SceneManager.LoadScene ("Level1");
	}

	// Show red background
	// cite : https://docs.unity3d.com/ScriptReference/WaitForSeconds.html
	public IEnumerator ShowHitEffect() {
		hitEffect.gameObject.SetActive (true);
		yield return new WaitForSeconds(0.2f);
		hitEffect.gameObject.SetActive (false);
	}

	// Set/ Reset life icon
	public void SetLife(int life) {
		GameObject[] lifes = GameObject.FindGameObjectsWithTag ("Life");
		Transform spawnPoints = new GameObject().transform;

		if (lifes.Length > 0) {
			for(int i = 0; i < lifes.Length; i++) {
				DestroyObject (lifes[i]);
			}
		}
	
		for(int i = 0; i < life; i++) {
			Vector2 _currentPos = new Vector2 ((1 * i) - 5.5f, 4f);
			Instantiate (lifeImage, _currentPos, spawnPoints.rotation);
		}
	}

	// Set/ Reset life icon
	public void SetBomb(int bomb) {
		GameObject[] bombs = GameObject.FindGameObjectsWithTag ("BombCount");
		Transform spawnPoints = new GameObject().transform;

		if (bombs.Length > 0) {
			for(int i = 0; i < bombs.Length; i++) {
				DestroyObject (bombs[i]);
			}
		}

		for(int i = 0; i < bomb; i++) {
			Vector2 _currentPos = new Vector2 ((1 * i) - 5.5f, -4f);
			Instantiate (bombImage, _currentPos, spawnPoints.rotation);
		}
	}

	// Spawn enemy
	void SpawnEnemy () {
		if (Game.Instance.NumEnemy >= maxEnemy)
			return;

		// Spawn! and increase number
		Instantiate (enemy);
		Game.Instance.NumEnemy++;
	}

	void SpawnBoss() {
		Instantiate (boss);
	}

	public void DefeatBoss() {
		GameObject[] gos = GameObject.FindGameObjectsWithTag ("boss_turret");
		foreach (GameObject go in gos) {
			DestroyObject (go);
			Destroy (go);
		}

		GameObject.FindGameObjectWithTag ("boss_broken").GetComponent<Explodable> ().explode ();
		levelClear = true;

		CancelInvoke ();
	}

	public void BombExplode() {
		StartCoroutine(ShowHitEffect());

		GameObject[] EnemyLaser = GameObject.FindGameObjectsWithTag ("EnemyLaser");

		if (EnemyLaser.Length > 0) {
			for(int i = 0; i < EnemyLaser.Length; i++) {
				DestroyObject (EnemyLaser[i]);
			}
		}

		GameObject[] BossMissile = GameObject.FindGameObjectsWithTag ("bossMissile");

		if (BossMissile.Length > 0) {
			for(int i = 0; i < BossMissile.Length; i++) {
				DestroyObject (BossMissile[i]);
			}
		}

		GameObject[] Enemies = GameObject.FindGameObjectsWithTag ("Enemy");

		if (Enemies.Length > 0) {
			for(int i = 0; i < Enemies.Length; i++) {
				Enemies [i].GetComponent<EnemyCollision> ().DestroyThis ();
			}
		}

		GameObject[] Enemies2 = GameObject.FindGameObjectsWithTag ("Enemy2");

		if (Enemies2.Length > 0) {
			for(int i = 0; i < Enemies2.Length; i++) {
				Enemies2 [i].GetComponent<Enemy2Collision> ().DestroyThis ();
			}
		}
	}
}
